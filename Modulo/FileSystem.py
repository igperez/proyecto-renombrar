import os
import shutil
import datetime


class FileSystem:

    def __init__(self, dir_name):
        self.dir_list = [dir_name]
        os.chdir(dir_name)

    def cd(self, dir_name):
        print(os.curdir)
        if (dir_name == "..."):
            if (len(self.dir_list) == 1):
                print("--> Se encuentra en el directorio raiz!\n")

            else:
                self.dir_list.pop
                os.chdir(self.dir_list[-1])
                print("--> Se cambió con éxito el directorio actual.\n")

        else:
            print(os.listdir(os.curdir))
            if (dir_name in os.listdir(os.curdir)):
                self.dir_list.append(dir_name)
                print("--> Se cambió con éxito el directorio actual.\n")

            else:
                print("--> Error! El directorio ingresado " +
                      "no es un directorio o algo asi.\n")

#    def compress(self, nombre_archivo="asd", fichero_origen=None,
#                 fichero_destino=os.getcwd()):
#        if (fichero_origen is None):
#            fichero_origen = self.directorios[-1]
#
#        shutil.make_archive(("%s/%s" % fichero_destino, nombre_archivo),
#                            "zip",
#                            fichero_origen)
#        print("--> El directorio '%s' se ha" +
#              "comprimido con éxito en '%s' \n" % fichero_origen,
#              fichero_destino)

    def dir(self):
        for dir_name, subdir_name, file_list in os.walk(os.curdir):
            print("   Directorio encontrado: %s" % dir_name)
            for fname in file_list:
                print("     %s" % fname)

    def rename(self, format_name):
        format_time = "%Y%m%d"
        i = 1

        for file_name in os.listdir(os.curdir):
            value = len(str(i))
            auxname = f"{os.curdir}\\{file_name}"
            auxtime = os.path.getctime(auxname)
            auxtime = datetime.datetime.fromtimestamp(auxtime)
            newname = (f"100_{auxtime.strftime(format_time)}_" +
                        "0"*(4-value) + "%s" % i)
            print(f"     {file_name} - {auxtime} - {newname}")
            i += 1
