from Modulo.FileSystem import FileSystem
import re


def main():
    aux = input(">> Ingrese el directorio raiz: \n>> ")
    
    try:
        buscador = FileSystem(aux)
    except Exception as e:
        print("#-> Se produjo un error. %s" % e)
    else:
        print("--> Se estableció con éxito el directorio raíz.\n")

    aux = input(">> ")

    while (aux.lower() != "exit"):
        if (re.match("cd ", aux, re.IGNORECASE)):
            buscador.cd(aux[3:])

        elif (aux.lower() == "dir"):
            buscador.dir()

        elif (aux.lower() == "compress"):
            print("--> Desea comprimir el directorio actual? yes/no\n")
            aux = input(">> ")

            # if (aux.lower() == "yes"):
                # buscador.compress()
            # else:
                # print("--> Compresión cancelada.\n")
        
        elif (re.match("rename ", aux, re.IGNORECASE)):
            buscador.rename(aux[7:])

        else:
            print("#>> Comando incorrecto! Intente nuevamente. \n")

        aux = input(">> ")


main()
